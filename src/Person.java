//package com.person;


import java.util.ArrayList;

public class Person {
    //gender enum
    public enum Genders {male, female, unknown}

    public String first_name;
    public String last_name;
    public String full_name;
    public String home_country;
    public Genders gender;
    public boolean prior_work_experience;
    public String undergraduate_institute;
    public ArrayList<String> hobbies;
    
    //constructor
    public Person(String first_name, String last_name, String home_country, String gender, boolean prior_work_experience, String undergraduate_institute, String []hobbies) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.home_country = home_country;
        this.full_name = first_name + " " + last_name;
        this.gender = generate_gender_from_String(gender);
        this.prior_work_experience = prior_work_experience;
        this.undergraduate_institute = undergraduate_institute;
        this.hobbies = new ArrayList<>();
        for(int i = 0; i < hobbies.length; i++) {
            this.hobbies.add(hobbies[i]);
        }
    }
    
    //getter
    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getFull_name() {
        return full_name;
    }

    public String getHome_country() {
        return home_country;
    }

    public Genders getGender() {
        return gender;
    }

    public boolean isPrior_work_experience() {
        return prior_work_experience;
    }

    public String getUndergraduate_institute() {
        return undergraduate_institute;
    }

    public ArrayList<String> getHobbies() {
        return hobbies;
    }

    //setter
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
        setFull_name();
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
        setFull_name();
    }

    private void setFull_name() {
        this.full_name = first_name + " " + last_name;
    }

    public void setHome_country(String home_country) {
        this.home_country = home_country;
    }

    public void setGender(String gender) {
        this.gender = generate_gender_from_String(gender);
    }

    public void setPrior_work_experience(boolean prior_work_experience) {
        this.prior_work_experience = prior_work_experience;
    }

    public void setUndergraduate_institute(String undergraduate_institute) {
        this.undergraduate_institute = undergraduate_institute;
    }

    public void setHobbies(ArrayList<String> hobbies) {
        this.hobbies = hobbies;
    }

    public void setHobbies(String [] hobbies) {
        this.hobbies = new ArrayList<>();
        for(int i = 0; i < hobbies.length; i++) {
            this.hobbies.add(hobbies[i]);
        }
    }

    //helper methods
    public Genders generate_gender_from_String(String gender) {
        gender = gender.toLowerCase();
        if(gender.equals("m") || gender.equals("male")) {
            return Genders.male;
        } else if (gender.equals("f") || gender.equals("female")) {
            return Genders.female;
        }
        return Genders.unknown;
    }

    public String he_or_she(boolean capitalize) {
        String ans = "";
        if(gender == Genders.male || gender == Genders.unknown) {
            ans += "he";
        } else {
            ans += "she";
        }
        if(capitalize) {
            ans = ans.substring(0,1).toUpperCase() + ans.substring(1);
        }
        return ans;
    }

    public String his_or_her(boolean capitalize) {
        String ans = "";
        if(gender == Genders.male || gender == Genders.unknown) {
            ans += "his";
        } else {
            ans += "her";
        }
        if(capitalize) {
            ans = ans.substring(0,1).toUpperCase() + ans.substring(1);
        }
        return ans;
    }

    public String toString_hobbies() {
        String ans = "";
        if(hobbies.size() == 0) {
            return "nothing";
        }
        if(hobbies.size() == 1) {
            ans += hobbies.get(0);
        } else if(hobbies.size() == 2) {
            ans += hobbies.get(0) + " and " + hobbies.get(1);
        } else {
            for(int i = 0; i < hobbies.size(); i++) {
                ans += hobbies.get(i);
                if(i != hobbies.size() - 1) {
                    if(i == hobbies.size() - 2) {
                        ans += " and ";
                    } else {
                        ans += ", ";
                    }
                }
            }
        }

        return ans;
    }

    //line generator
    public String intro_country() {
        return full_name + " is from " + home_country + ".";
    }

    

    public String intro_prior_work_experience() {
        String ans = "";
        ans += he_or_she(true);
        if(prior_work_experience) {
            ans += " has ";
        } else {
            ans += " does not have ";
        }
        ans += "prior work experience.";

        return ans;
    }
    
    public String intro_undergraduate_institute() {
        String ans = "";
        ans += he_or_she(true);
        ans += " received " + his_or_her(false) + " undergraduate from " + undergraduate_institute + ".";

        return ans;
    }



    public String intro_hobbies() {
        //When not in class, Shalin enjoys bodybuilding and dancing.
        String ans = "";
        ans += "When not in class, " + first_name + " enjoys " + toString_hobbies() + ".";
        return ans;
    }

    //major method
    public void intro_self() {
        String line = intro_country() + " " + intro_prior_work_experience() + " " + intro_undergraduate_institute() + " " + intro_hobbies();
        System.out.println(line);
    }

}
