//package com.person;

public class main {

    public static void main(String[] args) {
	// BlueDevil map
        BlueDevils blueDevils = new BlueDevils();
        //create Shalin
        String []hobbies = new String[] {"bodybuilding", "dancing"};
        BlueDevil blueDevil = new BlueDevil("Shalin","Shah", "India", "m", false, "DA-IICT", hobbies, "PHD", "ECE", false);
        blueDevils.addBlueDevil(blueDevil);
        //create Anil
        hobbies = new String[] {"climbing"};
        blueDevil = new BlueDevil("Anil","Ganti", "NJ", "m", true, "UNKNOWN", hobbies, "PHD", "ECE", false);
        blueDevils.addBlueDevil(blueDevil);
        //create Niral
        hobbies = new String[] {"playing tennis", "reading news"};
        blueDevil = new BlueDevil("Niral","Shal", "NJ", "m", false, "Rutgers University", hobbies, "ME", "ECE", false);
        blueDevils.addBlueDevil(blueDevil);
        //create Prof. Telford
        hobbies = new String[] {"golf", "sand volleyball", "swimming", "biking"};
        blueDevil = new BlueDevil("Ric","Telford", "US", "m", true, "Trinity University", hobbies, "PHD", "ECE", true);
        blueDevils.addBlueDevil(blueDevil);
        //create Haohong
        hobbies = new String[] {"jogging", "meditation"};
        blueDevil = new BlueDevil("Haohong","Zhao", "China", "m", false, "Tsinghua University", hobbies, "MS", "ECE", false);
        blueDevils.addBlueDevil(blueDevil);
        //search by 'whoIs'
        blueDevils.whoIs("Ric Telford");
        blueDevils.whoIs("Shalin Shah");
        blueDevils.whoIs("Niral Shal");
        blueDevils.whoIs("Anil Ganti");
        blueDevils.whoIs("Haohong Zhao");
        //example
        //Shalin Shah is from India and is a PhD ECE candidate. He does not have prior work experience. He received his undergraduate from DA-IICT. When not in class, Shalin enjoys bodybuilding and dancing.
    }
}
