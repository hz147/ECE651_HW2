//package com.person;

import java.util.HashMap;

public class BlueDevils {
    HashMap<String, BlueDevil> blueDevils;

    public BlueDevils() {
        blueDevils = new HashMap<>();
    }

    public BlueDevils(HashMap<String, BlueDevil> blueDevils) {
        this.blueDevils = blueDevils;
    }

    public void addBlueDevil(BlueDevil newPerson) {
        blueDevils.put(newPerson.full_name, newPerson);
    }

    public void whoIs(String full_name) {
        try{
            blueDevils.get(full_name).intro_self();
        } catch(NullPointerException e) {
            System.out.println("Cannot find " + full_name + " : " + e.getMessage());
        }
    }
}
