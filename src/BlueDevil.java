//package com.person;

public class BlueDevil extends Person {
    public enum Pursuing_degrees {MS, ME, PHD}
    public Pursuing_degrees pursuing_degree;
    public String school;
    public boolean instructor;

    //constructor
    public BlueDevil(String first_name, String last_name, String home_country, String gender, boolean prior_work_experience, String undergraduate_institute, String[] hobbies, String pursuing_degree, String school, boolean instructor) {
        super(first_name, last_name, home_country, gender, prior_work_experience, undergraduate_institute, hobbies);
        set_pursuing_degree(pursuing_degree);
        this.school = school;
        this.instructor = instructor;
    }

    //getter
    public Pursuing_degrees getPursuing_degree() {
        return pursuing_degree;
    }

    public String getSchool() {
        return school;
    }

    public boolean isInstructor() {
        return instructor;
    }

    //setter
    public void setPursuing_degree(String pursuing_degree) {
        this.pursuing_degree = set_pursuing_degree(pursuing_degree);
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public void setInstructor(boolean instructor) {
        this.instructor = instructor;
    }

    //helper methods
    public String a_or_an(String word) {
        String vowels = "aeiou";
        String line = "";
        if(vowels.contains(word.toLowerCase().substring(0,1))) {
            line += "an";
        } else {
            line += "a";
        }
        return line;
    }

    public Pursuing_degrees set_pursuing_degree(String pursuing_degree) {
        pursuing_degree = pursuing_degree.toLowerCase();
        if(pursuing_degree.equals("ms") || pursuing_degree.equals("m.s") || pursuing_degree.equals("m.s.") || pursuing_degree.equals("master of science")) {
            this.pursuing_degree = Pursuing_degrees.MS;
        } else if(pursuing_degree.equals("me") || pursuing_degree.equals("m.e") || pursuing_degree.equals("m.e.") || pursuing_degree.equals("master of engineer")) {
            this.pursuing_degree = Pursuing_degrees.ME;
        } else {
            this.pursuing_degree = Pursuing_degrees.PHD;
        }
        return this.pursuing_degree;
    }

    public String intro_blueDevil() {
        String line = intro_country();
        line = line.substring(0, line.length() - 1);
        line += " and is ";
        if(instructor) {
            line += a_or_an(school) + " " + school + " professor.";
        } else {
            line += a_or_an(pursuing_degree.toString()) + " " + pursuing_degree.toString() + " " + school + " candidate.";
        }

        return line;
    }

    //major method
    @Override
    public void intro_self() {
        String line = intro_blueDevil() + " " + intro_prior_work_experience() + " " + intro_undergraduate_institute() + " " + intro_hobbies();
        System.out.println(line);
    }
}
