# HW 2
## Clone
`git clone https://gitlab.oit.duke.edu/hz147/ECE651_HW2.git`
## Enter the directory
After Clone, run:
`cd ECE651_HW2`
## Compiling
To compile this, run:
`javac -d ./bin -sourcepath ./src ./src/main.java`

## Running
To run this code, use:
### Linux / OSX
`java -cp ./bin main`
`NOTICE!! no semicolon!!!!!!!!!!!!!!!!!!!!!!!!!!!!`
### Windows
`java -cp bin: main`
